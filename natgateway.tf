resource "google_compute_network" "default" {
  name = "my-network"
}

resource "google_compute_subnetwork" "default" {
  name          = "my-subnet"
  network       = google_compute_network.default.self_link
  ip_cidr_range = "10.0.0.0/14"
  region        = "us-central1"
}

resource "google_compute_router" "router" {
  name    = "router"
  region  = google_compute_subnetwork.default.region
  network = google_compute_network.default.self_link
  bgp {
    asn = 64514
  }
}

resource "google_compute_router_nat" "simple-nat" {
  name                               = "nat-1"
  router                             = google_compute_router.router.name
  region                             = "us-central1"
  nat_ip_allocate_option             = "AUTO_ONLY"
  source_subnetwork_ip_ranges_to_nat = "ALL_SUBNETWORKS_ALL_IP_RANGES"
}

