terraform {
  backend "gcs" {
    bucket = "tf-state-fifth-sol-251308"
    prefix = "terraform/state"
  }
}