variable "GOOGLE_CRED" {
  type = string
}
// Configure the Google Cloud provider
provider "google" {
  credentials = "${file(var.GOOGLE_CRED)}"
  project     = "fifth-sol-251308"
  region      = "us-west1"
}

resource "google_container_cluster" "primary" {
  name     = "super-fun-cluster"
  location = "us-central1"
  network  = "my-network"

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  ip_allocation_policy {
    use_ip_aliases = true
  }

  master_authorized_networks_config {
  }

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }

  private_cluster_config {
    enable_private_endpoint = true
    enable_private_nodes    = true
    master_ipv4_cidr_block  = "172.16.0.0/28"
  }
}

resource "google_container_node_pool" "primary_preemptible_nodes" {
  name       = "test-pool"
  location   = "us-central1"
  cluster    = "${google_container_cluster.primary.name}"
  node_count = 1

  node_config {
    machine_type = "n1-standard-1"

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

